-- 22102016
-- Verification Type

ALTER TABLE `verification_code` CHANGE `type` `type` INT(11) NOT NULL DEFAULT '0' COMMENT '0=New Account Verification, 1=Email Change';
