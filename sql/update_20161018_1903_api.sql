-- 18102016
-- API Table

CREATE TABLE `api` (
	`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'API Id',
	`login_id` INT(12) NOT NULL COMMENT 'Login Id',
	`title` INT(12) NOT NULL COMMENT 'Title of User',
	`api_key` VARCHAR(128) NOT NULL COMMENT 'API KEY',
	`expire_time` INT(12) NOT NULL COMMENT 'API Expiration Time',
	PRIMARY KEY (`id`)
) ENGINE = InnoDB;
