@echo off
setlocal EnableDelayedExpansion
SET LF=^


echo Local Address:
SET /p address=Enter Address:
SET /p pincode=Enter Pincode:
SET /p mobno=Enter Mobile Number:

echo Permanent Address:
SET /p address2=Enter Address:
SET /p pincode2=Enter Pincode:
SET /p mobno2=Enter Mobile Number:

SET /p sex=Enter Sex(M/F):
SET /p dob=Enter Date of Birth(dd-mm-yyyy):
SET /p birth_place=Enter Birth Place:
SET /p religion=Enter Religion:
SET /p category=Enter Category(Open/SC/ST):

SET /p var=<api_key.txt
SET response=
curl --data "api_key=%var%&address=%address%&pincode=%pincode%&mobile_no=%mobno%&address2=%address2%&pincode2=%pincode2%&mobile_no2=%mobno2%&sex=%sex%&date=%dob%&birth_place=%birth_place%&religion=%religion%&category=%category%" http://localhost:81/cmsys/system/update/profile/ > log.txt

REM GET Reponse
for /f "delims=" %%x in (log.txt) do (
  SET "response=!response!%%x!LF!"
)

echo !response!