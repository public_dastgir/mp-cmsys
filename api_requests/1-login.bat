@echo off
setlocal EnableDelayedExpansion
set LF=^


curl --data "api_key&api_login&username=COMP_s1_3&password=test" http://localhost:81/cmsys/system/login/ > log.txt
for /f "delims=" %%x in (log.txt) do (
  set "response=!response!%%x!LF!"
)

REM search for API_KEY POS
SET sstr="api_key"
SET stemp=!response!&SET pos=0
:loop
SET /a pos+=1
echo !stemp!|FINDSTR /b /c:"%sstr%" >NUL
IF ERRORLEVEL 1 (
	SET stemp=!stemp:~1!
	IF DEFINED stemp GOTO loop
	SET pos=0
)
REM Export the API KEy
REM 8+api_key+space+bracket+spaces+quote length
REM 8+7+2+10+1
SET api_key=!response:~28,40!
echo %api_key%> api_key.txt
echo API Key Exported to api_key.txt