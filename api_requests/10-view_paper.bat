@echo off
setlocal EnableDelayedExpansion
SET LF=^


SET /p paper_id=Enter Paper ID:
SET /p page_no=Enter Page Number:

SET /p var=<api_key.txt
SET response=
curl --data "api_key=%var%" http://localhost:81/cmsys/system/api/viewPaper/%paper_id%/%page_no%/ > log.png

REM GET Reponse
for /f "delims=" %%x in (log.png) do (
  SET "response=!response!%%x!LF!"
)

echo !response!