<!-- Error 404 Page -->
<?php
	if (API::get('SET')) {
		API::jsonAdd("error", "This Page doesn't exist");
		API::jsonAdd("error", "Please double check the url");
		API::jsonAdd("error_code", API_ERROR_404_PAGE);
		API::jsonPrint();
		API::stop();
	}
?>
<div class="cmsys-error1">
	<p>The Page You Requested doesn't exist.</p>
</div>