<?php
	if (!API::get('SET')) {
		if (!$menuAbove)
			echo '<div class="menu-btn">&#9776; Menu</div>';
		echo '<center><h1 id="page_heading" class="page_heading"></h1></center>';
	}
	// Get the requested url.
	$url = $_SERVER['REQUEST_URI'];
	// Remove '/cmsys/' from URL
	$subUrl = substr($url, 7);
	$passed = false;
	$CURRENT_PAGE_CONSTANT = "";
	// Check All Redirect Indexes to find best possible match.
	for ($i = 0; $i < count($redirect_indexes); $i = $i+4) {
		$testUrl = $redirect_indexes[$i+1];
		$title = $redirect_indexes[$i+3];
		$CURRENT_PAGE_CONSTANT = $redirect_indexes[$i];
		$testUrl = '/'.str_replace('/', '\/', $testUrl).'/';
		//print($testUrl.":".$subUrl.":". preg_match($testUrl, $subUrl) ."<br/>");
		// Match the String.
		if (preg_match($testUrl, $subUrl)) {
			$passed = true;
			$role = $redirect_indexes[$i+2];
			//print($role."<br/>");
			if ($role == '')
				break;
			// Check if User can perform the role.
			if ($login->roleValid($role)) {
				$passed = true;
				/** API pages can only be accessed by API. */
				if (strpos($subUrl, '/api/')) {
					if (API::get('SET')) {
						if (!API::get('VERIFIED')) {
							API::invalidKeyError();
							API::jsonPrint();
							API::stop();
						}
					} else {
						$passed = false;
					}
				}
				break;
			} else {	// User cannot perform the role, set passed to false.
				$passed = false;
			}
		}
	}
	if ($passed == false) {	// Invalid Page/Unauthorized
		include 'error404.php';
	} else {
		// Authorized, get the Template Name.
		$array = RedirectHandler::getIncludeName($url);
		$fileToInclude = $array[0];
		$matches = $array[1];
		// If File doesn't exist, show 404 page.
		if (!file_exists($fileToInclude)) {
			$CURRENT_PAGE_CONSTANT = "";
			include 'error404.php';
		} else {
			// Include the Template
			include $fileToInclude;
		}
	}
?>