<?php
/**
 * Get Subjects and Marks
 */
// Sends the Response.
function sendResponse($response) {
	echo "<b id='wwww'>$response</b>";
}
// Get the Sem and branch.
if (isset($_POST["sem"]) && !empty($_POST["sem"])) {
	$sem = $_POST["sem"];
}
if (isset($_POST["user"]) && !empty($_POST["user"])) {
	$user = $_POST["user"];
}
/** Generate Valid Semester Value for API via numeric */
if (API::get('VERIFIED')) {
	$user = $login->id;
	$sem = intval($sem);
	$semArray = array('Sem I', 'Sem II', 'Sem III', 'Sem IV', 'Sem V', 'Sem VI', 'Sem VII', 'Sem VIII');
	$sem = $semArray[$sem-1];
}
// IF Branch/Sem is not set, send no response.
// Also, User can view only their own attendance.
if (!isset($user) || !isset($sem) || $user != $login->id) {
	if (API::get('SET')) {
		API::jsonAdd('error', 'Invalid Semester Entered');
		API::jsonAdd('error_code', API_ERROR_INVALID_SEM);
		API::jsonPrint();
		API::stop();
	}
	sendResponse("");
} else {
	// SQL Query to fetch neccessary data about Attendnace.
	$sql = "SELECT `login`.`name` AS `added_by`, `am`.`topics`, `am`.`attendance` as `total_lecture`,
					`am`.`subject_code`, `am`.`date`, `as`.`attended`
					FROM `attendance_main` `am`
					RIGHT JOIN `attendance_student` AS `as` ON
					`am`.`id`=`as`.`attendance_id` AND `as`.`student_id`='$user'
					RIGHT JOIN `login` ON
					`am`.`teacher_id`=`login`.`id`
					WHERE `am`.`semester`='$sem'";
	$login->DB->query($sql);
	// Generate Array and save result into it.
	$a = array();

	if ($login->DB->result->num_rows > 0) {
		while (($res = $login->DB->result->fetch_assoc())) {
			$res['subject'] = $login->allSubjects[array_search($res['subject_code'], $login->allSubjects) + 1];
			$a[] = $res;
		}
		/** Generate Valid output for API */
		if (API::get('VERIFIED')) {
			$fields = array('subject', 'topics', 'added_by', 'date', 'total_lecture', 'attended');
			for ($i = 0; $i < count($a); $i++) {
				for ($j = 0; $j < count($fields); $j++) {
					/** Convert total_lecture to integer, and attended to boolean */
					if ($fields[$j] == 'total_lecture') {
						$a[$i][$fields[$j]] = intval($a[$i][$fields[$j]]);
					} else if ($fields[$j] == 'attended') {
						if ($a[$i][$fields[$j]] == 1)
							$a[$i][$fields[$j]] = true;
						else
							$a[$i][$fields[$j]] = false;
					}
					API::jsonAdd($i+1, array($fields[$j] => $a[$i][$fields[$j]]));
				}
			}
			API::jsonPrint();
			API::stop();
		} else {
			// Json Encode, so js can read it.
			sendResponse(json_encode($a));
		}
	} else {
		if (API::get('SET')) {
			$login->addInfo('No Attendance Information Found');
			API::jsonPrint();
			API::stop();
		}
		sendResponse(json_encode(""));
	}
}

?>