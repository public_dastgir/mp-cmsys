<?php
/** @var bool UserID is fetched from POST? */
$userPost = false;
/** @var bool RedirectConstant is fetched from POST? */
$redirectSet = false;
// Image User ID
if ($login->title >= 4 && isset($_POST) && isset($_POST["image_uid"])) {
	$userId = intval($_POST["image_uid"]);
	$userPost = true;
} else {
	$userId = $login->id;
}
// Get Redirect Constant
if (isset($_POST["redirect"])) {
	if (defined($_POST["redirect"])) {
		$redirectConstant = $_POST["redirect"];
		$redirectSet = true;
	}
}
// Allowed Image Type
$allowedTypes = array(IMAGETYPE_PNG);
if (!isset($_FILES["profile-image-upload"]) || !isset($_FILES["profile-image-upload"]["tmp_name"])) {
	$login->addError("No File Uploaded");
	$login->errorRedirect("CMSYS_UPDATE_PROFILE");
}
$tmpName = $_FILES["profile-image-upload"]["tmp_name"];
// Get Uploaded File Image Type.
$detectedType = exif_imagetype($tmpName);
// Check if it's same as of AllowedType.
$error = !in_array($detectedType, $allowedTypes);
$check = getimagesize($tmpName);


$fileHandler = new FileHandler(UPLOAD_PICTURE);
if(!$error && $check !== false) {	
	$uploadOk = 1;
} else {
	// Not in PNG format.
	$fileHandler->addError("Please Upload file in PNG format.");
	$fileHandler->errorRedirect("CMSYS_UPDATE_PROFILE");
}
// Upload Picture with suffix ID.
$fileHandler->upload(file_get_contents($tmpName), $userId);
// Log
$login->logs->insertLogByType(CMSYS_LOG_UPDATE_PICTURE);
$fileHandler->addInfo("Picture Changed Successfully");
if ($redirectSet == false)
	$fileHandler->redirect("CMSYS_UPDATE_PROFILE");
else
	$fileHandler->redirect($redirectConstant, $userId)
?>