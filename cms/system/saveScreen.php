<?php
// Save Width and Height
if(isset($_POST['width']) && isset($_POST['height'])) {
    $_SESSION['screen_width'] = intval($_POST['width']);
    $_SESSION['screen_height'] = intval($_POST['height']);
    echo json_encode(array('outcome'=>'success', 'w' => $_POST['width'], 'h' => $_POST['height']));
} else {
    echo json_encode(array('outcome'=>'error','error'=>"Couldn't save dimension info"));
}
?>