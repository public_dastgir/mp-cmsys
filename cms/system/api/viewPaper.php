<?php
	if (!API::get('SET')) {
		exit();
	}
	/**
	 * View Paper(For Students) Page
	 */
	// Check the URL.
	if (count($matches) != 6) {
		$login->addError("Invalid Page number.");
		$login->errorRedirect("CMSYS_PROFILE");
	}
	// Get Page Number.
	$paperUID = intval($matches[4]);
	$pageNo = intval($matches[5]);
	
	$login->DB->query("SELECT * FROM `paper_student` WHERE `id`='$paperUID' AND `student_id`='". $login->id ."'");
	if ($login->DB->result->num_rows == 0) {
		$login->addError("Invalid Paper");
		$login->errorRedirect("CMSYS_PROFILE");
	}
	$res = $login->DB->result->fetch_assoc();
	$paperId = $res['paper_id'];
	$studId = $login->id;
	// Initialize Paper File Handler.
	$fileHandler = new FileHandler(UPLOAD_PAPER);
	// Check if Paper Exists.
	$exists = $fileHandler->deepDIR(array($paperId, $studId), false);
	if (!$exists) {
		$login->addError("Papers not Uploaded, Please Contact Staff Member.");
		$login->errorRedirect("CMSYS_PROFILE");
	}
	// Get Number of files.
	$count = $fileHandler->count();
	// Get Paper Image.
	$paper = RedirectHandler::getRedirectURL('CMSYS_GET_PAPER', $studId."/".$paperId."/".$pageNo);
	// Error if invalid page number.
	if ($pageNo <= 0 || $pageNo > $count) {
		$login->addError("Invalid Page Number");
		$login->errorRedirect("CMSYS_PROFILE");
	}

	// Create Image Resource
	$img = $fileHandler->get($pageNo);
	$im = imagecreatefromstring($img);
	$im = imagescale($im, 1024, 600);
	// Get the Logs and print text on image
	$sql = "SELECT `x`, `y`, `color`, `text`, `count` FROM `paper_clog` WHERE `student_id`='$studId' AND `paper_id`='$paperId' AND `page_no`='$pageNo'";
	$login->DB->query($sql);
	if ($login->DB->result->num_rows > 0) {
		$res = $login->DB->result->fetch_assoc();
		$x = unserialize($res['x']);
		$y = unserialize($res['y']);
		$color = unserialize($res['color']);
		$text = unserialize($res['text']);
		$count = intval($res['count']);
		for ($i = 0; $i < $count; $i++) {
			// Get Valid RGB Color Value
			if ($color[$i] == "red") {
				$r = 255;
				$g = 0;
				$b = 0;
			} else {
				$c = hexdec($color[$i]);
				$r = $c&0xFF0000;
				$g = $c&0x00FF00;
				$b = $c&0x0000FF;
			}
			$cor = imagecolorallocate($im, $r, $g, $b);
			// Create Image File
			putenv('GDFONTPATH=' . realpath('.'));
			imagettftext($im, 30, 0, $x[$i], $y[$i], $cor, 'arial.ttf', $text[$i]);
		}
	}
	// Output the Image File
	ob_start();
	imagepng($im);
	$image_string = ob_get_contents();
	ob_end_flush();
	print($image_string);
	API::stop();
	exit();
?>