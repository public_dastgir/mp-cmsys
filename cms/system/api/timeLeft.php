<?php
	if (API::get('SET')) {
		if (API::get('VERIFIED')) {
			API::jsonAdd('time_left', API::get('TIME_LEFT'));
		} else {
			API::invalidKeyError();
		}
		API::jsonPrint();
		API::stop();
	}
?>