<?php
/**
 * Get Subjects and Marks
 */
// Sends the Response.
function sendResponse($response) {
	echo "<b id='wwww'>$response</b>";
}
// Get the Sem and branch.
if (isset($_POST["sem"]) && !empty($_POST["sem"])) {
	$sem = $_POST["sem"];
}

/** Generate Valid Semester Value for API via numeric */
if (API::get('VERIFIED')) {
	$user = $login->id;
	$sem = intval($sem);
	$semArray = array('Sem I', 'Sem II', 'Sem III', 'Sem IV', 'Sem V', 'Sem VI', 'Sem VII', 'Sem VIII');
	$sem = $semArray[$sem-1];
}

// IF Branch/Sem is not set, send no response.
if (!isset($sem)) {
	if (API::get('SET')) {
		API::jsonAdd('error', 'Invalid Semester Entered');
		API::jsonAdd('error_code', API_ERROR_INVALID_SEM);
		API::jsonPrint();
		API::stop();
	}
	sendResponse("");
} else {
	// SQL Query to fetch neccessary data about Marks.
	$sql = "SELECT `login`.`name` AS `checked_by`,`ps2`.`id`, `pm`.`name`, `pm`.`date`, `pm`.`marks`,
				`ps2`.`tmarks`, `pm`.`subject_code`,
				(SELECT  COUNT(*)
					FROM `paper_student` AS `ps1`
        			WHERE `ps1`.`tmarks` >= `ps2`.`tmarks` AND `ps1`.`paper_id`=`pm`.`id`
        		) AS `rank`
				FROM `paper_main` `pm`
				RIGHT JOIN `paper_student` AS `ps2` ON
				`pm`.`id`=`ps2`.`paper_id` AND `ps2`.`student_id`='". $login->id ."'
				AND `ps2`.`checked`='2'
				RIGHT JOIN `login` ON 
				`ps2`.`checked_by`=`login`.`id`
				WHERE `pm`.`semester`='$sem'";
	$login->DB->query($sql);
	// Generate Array and save result into it.
	$a = array();

	if ($login->DB->result->num_rows > 0) {
		while (($res = $login->DB->result->fetch_assoc())) {
			$res['subject'] = $login->allSubjects[array_search($res['subject_code'], $login->allSubjects) + 1];
			$a[] = $res;
		}
		/** Generate Valid output for API */
		if (API::get('VERIFIED')) {
			$fields = array('id', 'name', 'subject', 'checked_by', 'marks', 'tmarks', 'rank');
			$fields_replace = array('paper_id', 'name', 'subject', 'checked_by', 'total_marks', 'marks_obtained', 'rank');
			for ($i = 0; $i < count($a); $i++) {
				for ($j = 0; $j < count($fields); $j++) {
					/** Convert total_lecture to integer, and attended to boolean */
					if ($fields[$j] == 'id' || $fields[$j] == 'marks' || $fields[$j] == 'tmarks' || $fields[$j] == 'rank') {
						$a[$i][$fields[$j]] = intval($a[$i][$fields[$j]]);
					}
					API::jsonAdd($i+1, array($fields_replace[$j] => $a[$i][$fields[$j]]));
				}
			}
			API::jsonPrint();
			API::stop();
		} else {
			// Json Encode, so js can read it.
			sendResponse(json_encode($a));
		}
	} else {
		if (API::get('SET')) {
			$login->addInfo('No Marks Information Found');
			API::jsonPrint();
			API::stop();
		}
		sendResponse(json_encode(""));
	}
}

?>