<?php
require_once(dirname(__FILE__).'/../include.php');

$c = new ConstantHandler();
$c->define(
				'API_LOGIN_API_EXISTS',     // API Already Exists, but user attempted new login.
				'API_LOGIN_NEW_API',        // API Generated when new login was requested.

				'API_ERROR_NOT_AUTHORIZED', // Unauthorized Access
				'API_ERROR_INVALID_PAGE',   // Invalid Page Accessed
				'API_ERROR_REDIRECTION',    // Redirection
				'API_ERROR_NO_PAGE',        // Page Number not mentioned in URL
				'API_ERROR_404_PAGE',       // Invalid Page
				'API_ERROR_INVALID_SEM',    // Invalid Semester Entered in attendance
				'API_ERROR_NO_FILE',        // No Files to Display
				'API_END'
			);

/**
 * This Class Handles functions related to API
 * API can be used in many applications to
 * interface CMSys in other programming languages.
 */
class API extends ErrorHandler {
	/**
	 * Defines a Constant with prefix API_
	 * @method set
	 * @param  string $key   Constant Name
	 * @param  mixed $value Constant Value
	 */
	public static function set($key, $value) {
		$key = 'API_'. strtoupper($key);
		if (defined($key)) {
			unset($key);
		}
		define($key, $value);
	}

	/**
	 * Returns the Constant defined by @see API::set Method
	 * @method get
	 * @param  string   $key Constant Name
	 * @return mixed Constant Value, false if constant doesn't exist.
	 */
	public static function get($key) {
		$key = 'API_'. strtoupper($key);
		if (defined($key))
			return constant($key);
		return false;
	}

	/**
	 * Add some field into array.
	 * @method jsonAdd
	 * @param  mixed	$index	Index of Array
	 * @param  string	$value	Value to add in index
	 */
	public static function jsonAdd($index, $value) {
		global $API_json;
		if (isset($API_json[$index])) {
			$API_json[$index][] = $value;
		} else {
			$API_json[$index] = array($value);
		}
	}

	/**
	 * Returns the index set via jsonAdd method.
	 * returns undefined json_encoded form message, if index is not defined.
	 * @method jsonGet
	 * @param  mixed  $index Index Value
	 * @return mixed         Output from array
	 */
	public static function jsonGet($index) {
		global $API_json;
		if (isset($API_json[$index])) {
			return $API_json[$index];
		}
		return json_encode(array($index => "undefined"));
	}

	/**
	 * Prints the API Messages in json format.
	 * @method jsonPrint
	 */
	public static function jsonPrint() {
		global $API_json;
		ob_clean();
		flush();
		print(json_encode($API_json, JSON_PRETTY_PRINT));
		$API_json = array();
	}

	/**
	 * Generates Random SHA1 String
	 * @method sha1
	 * @param  integer $length Length of SHA1 String
	 * @return string         SHA1 Encoded Random String
	 */
	private function sha1($length) {
		$max = ceil($length / 40);
		$random = '';
		for ($i = 0; $i < $max; $i ++) {
			$random .= sha1(microtime(true).mt_rand(10000,90000));
		}
		return substr($random, 0, $length);
	}

	/**
	 * Performs any last action before exiting the PHP script.
	 * @method stop
	 */
	public static function stop() {
		exit();
	}

	/**
	 * Generates a New API Key if not already generated, else output's current API Key
	 * @method insertLogin
	 * @param  class      $login Login Class
	 */
	public static function insertLogin($login) {
		$login->DB->query("SELECT `id`, `api_key` FROM `api` WHERE `login_id`='". $login->id ."' AND `expire_time`>'". time() ."'");
		if ($login->DB->result->num_rows > 0) {
			$row = $login->DB->result->fetch_assoc();
			$apiKey = $row['api_key'];
			$infoCode = API_LOGIN_API_EXISTS;
		} else {
			$apiKey = self::sha1(40);
			$infoCode = API_LOGIN_NEW_API;
			$login->DB->query("INSERT INTO `api` (`login_id`, `title`, `api_key`, `expire_time`) VALUES ('". $login->id ."', '". $login->title ."', '". $apiKey ."', '". (time()+3600) ."')");
		}
		API::jsonAdd('api_key', $apiKey);
		API::jsonAdd('info_code', $infoCode);
		API::jsonPrint();
		API::stop();
	}

	public static function invalidKeyError() {
		API::jsonAdd("error", "Invalid Verification Key");
		API::jsonAdd("error_code", API_ERROR_NOT_AUTHORIZED);
	}
}
?>