<?php
// Load All Configs
require_once dirname(__FILE__).'/config/main.php';
// Load All Traits
foreach (glob(dirname(__FILE__).'/modules/traits/*.php') as $filename) {
    require_once $filename;
}
// Load All Modules
require_once dirname(__FILE__).'/modules/constantHandler.php';	// Priority
require_once dirname(__FILE__).'/modules/errorHandler.php';	// Priority
foreach (glob(dirname(__FILE__).'/modules/*.php') as $filename) {
    require_once $filename;
}

/** API Check */
/**
 * Check for API Key
 * Also, Check if API Key is valid, and store them into 3 variables.
 * Constants:
 * VERIFIED => API Verified the User
 * TITLE => User Title
 * TIME_LEFT => Time Left for API Key to Expire
 * KEY => API Key
 * LOGIN => Checks if API login request is present
 * Variable:
 * $API_json => All Information stored in array format.
 */
if (isset($_POST["api_key"])) {
	$login = new Login('CMSYS_INDEX');
	$tempDB = new DB();
	API::set('KEY', $_POST["api_key"]);
	$isAPIset = false;
	if (isset($_POST["api_login"])) {
		API::set('LOGIN', true);
	} else if ($_POST["api_key"] != NULL) {
		$tempDB->escape($_POST["api_key"]);
		$tempDB->query("SELECT `login_id`, `title`, `expire_time`-". time() ." AS `time_left` FROM `api` WHERE `api_key`='". API::get('KEY') ."' AND `expire_time`>'". time() ."'");
		if ($tempDB->result->num_rows > 0) {
			$rows = $tempDB->result->fetch_assoc();
			API::set('TITLE', intval($rows["title"]));
			API::set('TIME_LEFT', intval($rows["time_left"]));

			ErrorHandler::cError();
			$isAPIset = true;
			$login->checkUser($rows['login_id'], 2);
		}
		API::set('LOGIN', false);
	}
	API::set('VERIFIED', $isAPIset);
	API::set('SET', true);
	$API_json = array();
}

if (API::get('SET') == false) {
	// Load the FontAwesome css.
	echo '<link rel="stylesheet" type="text/css" href="'. $rootDir .'/css/font-awesome.min.css">';
	echo '<link rel="stylesheet" type="text/css" href="'. $rootDir .'/css/roboto.css">';
}
?>